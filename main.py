#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example driver code for testing software library. Not part of the library itself.

@author: Aaron Griffin
"""

import perform as p
import md
import plot as pl
import accuracy as acc

def main():
    
    '''
    Uncomment and comment out driver code segments below, depending on what needs
    testing
    '''
    
    # Driver code for testing performance with textual output
    
    '''
    ec_perf = p.euler_cromer_perf(10, 15, 15, 1e-1, 10)
    print("euler_cromer_md time = {}".format(ec_perf))
    print()
    
    v_perf = p.verlet_perf(10, 15, 15, 1e-1, 10)
    print("verlet_md time = {}".format(v_perf))
    print()
    
    vv_perf = p.velocity_verlet_perf(10, 15, 15, 1e-1, 10)
    print("velocity_verlet_md time = {}".format(vv_perf))
    print()
    '''
    
    # Driver code for testing textual representation of md sims directly
    
    '''
    print("verlet_md textual test:")
    print()
    md.verlet_md_text(10, 15, 15, 1e-2, 40)
    print('\n')
    
    print("velocity_verlet_md textual test:")
    print()
    md.velocity_verlet_md_text(15, 15, 10, 1e-2, 100)
    print('\n')

    
    print("euler_cromer_md textual test:")
    print()
    md.euler_cromer_md_text(10, 15, 15, 1e-2, 30)
    print('\n')
    
    print("leapfrog_md textual test:")
    print()
    md.leapfrog_md_text(10, 15, 15, 1e-5, 100)
    print('\n')
    '''
    '''
    print('leapfrog_md_naive_pair textual test:')
    print()
    md.leapfrog_md_naive_pair_text(10, 15, 15, 1e-5, 110, 2.5)
    print('\n')
    '''
    '''
    print('leapfrog_md_verlet_list textual test:')
    print()
    md.leapfrog_md_verlet_list_text(10, 15, 15, 1e-5, 300, 100.0, 0.5)
    print('\n')
    '''
    
    # Driver code for plotting required figures and printing related measurements
    
    #pl.plot_perf_print_avg(10, 30)
    
    #pl.plot_te_conservation(10, 1e-8)
    
    #pl.plot_sri_te_conservation(10, 1e-5)
    
    #pl.plot_sri_perf_print_avg(10, 190)
    
    
    # Driver code for printing average total energy difference to delta_t = 1
    '''
    ip, fp, iv, fv, cv, lte = md.leapfrog_md(10, 15, 15, 1e-5, 100)
    lf_te_avg_diff = acc.total_energy_avg_diff(lte)
    print('Leapfrog avg total energy diff = {}\n'.format(lf_te_avg_diff))
    
    ip, fp, iv, fv, cv, vte = md.verlet_md(10, 15, 15, 1e-5, 100)
    v_te_avg_diff = acc.total_energy_avg_diff(vte)
    print('Verlet avg total energy diff = {}\n'.format(v_te_avg_diff))
    
    ip, fp, iv, fv, cv, vvte = md.velocity_verlet_md(10, 15, 15, 1e-5, 100)
    vv_te_avg_diff = acc.total_energy_avg_diff(vvte)
    print('Velocity Verlet avg total energy diff = {}\n'.format(vv_te_avg_diff))
    '''
    
    # Driver code for printing average of average total energy difference over n runs, for numerical integrators
    '''
    lf_avg_diff_n_runs = []
    v_avg_diff_n_runs = []
    vv_avg_diff_n_runs = []
    
    n = 10
    
    for i in range(n):
        ip, fp, iv, fv, cv, lte = md.leapfrog_md(10, 15, 15, 1e-5, 30)
        lf_te_avg_diff = acc.total_energy_avg_diff(lte)
        lf_avg_diff_n_runs.append(lf_te_avg_diff)
        
        ip, fp, iv, fv, cv, vte = md.verlet_md(10, 15, 15, 1e-5, 30)
        v_te_avg_diff = acc.total_energy_avg_diff(vte)
        v_avg_diff_n_runs.append(v_te_avg_diff)
        
        ip, fp, iv, fv, cv, vvte = md.velocity_verlet_md(10, 15, 15, 1e-5, 30)
        vv_te_avg_diff = acc.total_energy_avg_diff(vvte)
        vv_avg_diff_n_runs.append(vv_te_avg_diff)
        
    print('Leapfrog avg of avg over n runs = {}'.format(acc.calc_avg(lf_avg_diff_n_runs)))
    print()
    print('Verlet avg of avg over n runs = {}'.format(acc.calc_avg(v_avg_diff_n_runs)))
    print()
    print('Velocity Verlet avg of avg over n runs = {}'.format(acc.calc_avg(vv_avg_diff_n_runs)))
    print()
    '''
    
    # Driver code for printing average of average total energy difference over n runs, for short-range interaction algorithms
    lfnp_avg_diff_n_runs = []
    lfvl_avg_diff_n_runs = []
    
    n = 10
    
    for i in range(n):
        ip, fp, iv, fv, cv, lnpte = md.leapfrog_md_naive_pair(10, 15, 15, 1e-11, 100, 3.0)
        lfnp_te_avg_diff = acc.total_energy_avg_diff(lnpte)
        lfnp_avg_diff_n_runs.append(lfnp_te_avg_diff)
        
        ip, fp, iv, fv, cv, lfvlte = md.leapfrog_md_verlet_list(10, 15, 15, 1e-11, 100, 2.5, 0.5)
        lfvl_te_avg_diff = acc.total_energy_avg_diff(lfvlte)
        lfvl_avg_diff_n_runs.append(lfvl_te_avg_diff)
        
    print('Leapfrog with naive pair, avg of avg over n runs = {}'.format(acc.calc_avg(lfnp_avg_diff_n_runs)))
    print()
    print('Leapfrog with verlet list, avg of avg over n runs = {}'.format(acc.calc_avg(lfvl_avg_diff_n_runs)))
    print()
    


if __name__ == "__main__":
    main()