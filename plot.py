#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module containing functions for plotting figures and printing measurements comparing 
the various algorithms used in this library. 

@author: Aaron Griffin
"""

import matplotlib.pyplot as plt
import numpy as np
import perform as p
import accuracy as acc
import md
       

def plot_perf(it, sim_steps):
    num_its = np.array([])
    lp_perfs = np.array([])
    v_perfs = np.array([])
    vv_perfs = np.array([])
    
     
    lp = 0.0
    vp = 0.0
    vvp = 0.0
    for i in range(it):
        lp = p.leapfrog_perf(10, 15, 15, 1e-5, sim_steps)
        vp = p.verlet_perf(10, 15, 15, 1e-5, sim_steps)
        vvp = p.velocity_verlet_perf(10, 15, 15, 1e-5, sim_steps)
        
        num_its = np.append(num_its, [i+1])
        lp_perfs = np.append(lp_perfs, [lp])
        v_perfs = np.append(v_perfs, [vp])
        vv_perfs = np.append(vv_perfs, [vvp])
    
    fig, ax = plt.subplots()
    ax.plot(num_its, lp_perfs, label='leapfrog')
    ax.plot(num_its, v_perfs, label='verlet')
    ax.plot(num_its, vv_perfs, label='velocity verlet')
    ax.set_xlabel('Run Number')
    ax.set_ylabel('Running Time (secs)')
    ax.set_title('Running Time of MD Simulations with ' + str(sim_steps) + ' Time Steps')
    ax.legend()

    

def plot_perf_print_avg(it, sim_steps):
    num_its = np.array([])
    lp_perfs = np.array([])
    v_perfs = np.array([])
    vv_perfs = np.array([])
    
     
    lp = 0.0
    vp = 0.0
    vvp = 0.0
    lp_tot = 0.0
    vp_tot = 0.0
    vvp_tot = 0.0
    for i in range(it):
        lp = p.leapfrog_perf(10, 15, 15, 1e-5, sim_steps)
        vp = p.verlet_perf(10, 15, 15, 1e-5, sim_steps)
        vvp = p.velocity_verlet_perf(10, 15, 15, 1e-5, sim_steps)
        
        lp_tot += lp
        vp_tot += vp
        vvp_tot += vvp
        
        num_its = np.append(num_its, [i+1])
        lp_perfs = np.append(lp_perfs, [lp])
        v_perfs = np.append(v_perfs, [vp])
        vv_perfs = np.append(vv_perfs, [vvp])
    
    fig, ax = plt.subplots()
    ax.plot(num_its, lp_perfs, label='leapfrog')
    ax.plot(num_its, v_perfs, label='verlet')
    ax.plot(num_its, vv_perfs, label='velocity verlet')
    ax.set_xlabel('Run Number')
    ax.set_ylabel('Running Time (secs)')
    ax.set_title('Running Time of MD Simulations with ' + str(sim_steps) + ' Time Steps')
    ax.legend()
    
    lp_average = lp_tot / float(it)
    vp_average = vp_tot / float(it)
    vvp_average = vvp_tot / float(it)
    
    print("Average runtime: leapfrog, {} runs, {} time steps = {}".format(it, sim_steps, lp_average))
    print()
    print("Average runtime: verlet, {} runs, {} time steps = {}".format(it, sim_steps, vp_average))
    print()
    print("Average runtime: velocity verlet, {} runs, {} time steps = {}".format(it, sim_steps, vvp_average))
    print()



def plot_te_conservation(it, delta_t):
    num_its = np.array([])
    lf_avg_te_diff_list = np.array([])
    v_avg_te_diff_list = np.array([])
    vv_avg_te_diff_list = np.array([])
    
     
    lf_avg_te_diff = 0.0
    v_avg_te_diff = 0.0
    vv_avg_te_diff = 0.0
    for i in range(it):
        ip, fp, iv, fv, cv, lf_te = md.leapfrog_md(10, 15, 15, delta_t, 30)
        lf_avg_te_diff = acc.total_energy_avg_diff(lf_te)
        
        ip, fp, iv, fv, cv, v_te = md.verlet_md(10, 15, 15, delta_t, 30)
        v_avg_te_diff = acc.total_energy_avg_diff(v_te)
        
        ip, fp, iv, fv, cv, vv_te = md.velocity_verlet_md(10, 15, 15, delta_t, 30)
        vv_avg_te_diff = acc.total_energy_avg_diff(vv_te)
        
        num_its = np.append(num_its, [i+1])
        lf_avg_te_diff_list = np.append(lf_avg_te_diff_list, [lf_avg_te_diff])
        v_avg_te_diff_list = np.append(v_avg_te_diff_list, [v_avg_te_diff])
        vv_avg_te_diff_list = np.append(vv_avg_te_diff_list, [vv_avg_te_diff])
    
    fig, ax = plt.subplots()
    ax.plot(num_its, lf_avg_te_diff_list, label='leapfrog')
    ax.plot(num_its, v_avg_te_diff_list, label='verlet')
    ax.plot(num_its, vv_avg_te_diff_list, label='velocity verlet')
    ax.set_xlabel('Run Number')
    ax.set_ylabel('Avg. Total Energy Difference to t = 1')
    ax.set_title('TE Conservation: Time Step Size of ' + str(delta_t))
    ax.legend()
    
    
def plot_sri_te_conservation(it, delta_t):
    num_its = np.array([])
    lfnp_avg_te_diff_list = np.array([])
    lfvl_avg_te_diff_list = np.array([])
    
     
    lfnp_avg_te_diff = 0.0
    lfvl_avg_te_diff = 0.0
    for i in range(it):
        ip, fp, iv, fv, cv, lfnp_te = md.leapfrog_md_naive_pair(10, 15, 15, delta_t, 150, 3.0)
        lfnp_avg_te_diff = acc.total_energy_avg_diff(lfnp_te)
        
        ip, fp, iv, fv, cv, lfvl_te = md.leapfrog_md_verlet_list(10, 15, 15, delta_t, 150, 2.5, 0.5)
        lfvl_avg_te_diff = acc.total_energy_avg_diff(lfvl_te)
        
        num_its = np.append(num_its, [i+1])
        lfnp_avg_te_diff_list = np.append(lfnp_avg_te_diff_list, [lfnp_avg_te_diff])
        lfvl_avg_te_diff_list = np.append(lfvl_avg_te_diff_list, [lfvl_avg_te_diff])
    
    fig, ax = plt.subplots()
    ax.plot(num_its, lfnp_avg_te_diff_list, label='leapfrog w/ naive pair')
    ax.plot(num_its, lfvl_avg_te_diff_list, label='leapfrog w/ verlet list')
    ax.set_xlabel('Run Number')
    ax.set_ylabel('Avg. Total Energy Difference to t = 1')
    ax.set_title('TE Conservation: Time Step Size of ' + str(delta_t))
    ax.legend()
    

def plot_sri_perf_print_avg(it, sim_steps):
    num_its = np.array([])
    lfnp_perfs = np.array([])
    lfvl_perfs = np.array([])
    
     
    lfnpp = 0.0
    lfvlp = 0.0
    lfnpp_tot = 0.0
    lfvlp_tot = 0.0
    for i in range(it):
        lfnpp = p.leapfrog_naive_pair_perf(10, 15, 15, 1e-5, sim_steps, 3.0)
        lfvlp = p.leapfrog_verlet_list_perf(10, 15, 15, 1e-5, sim_steps, 2.5, 0.5)
        
        lfnpp_tot += lfnpp
        lfvlp_tot += lfvlp
        
        num_its = np.append(num_its, [i+1])
        lfnp_perfs = np.append(lfnp_perfs, [lfnpp])
        lfvl_perfs = np.append(lfvl_perfs, [lfvlp])
    
    fig, ax = plt.subplots()
    ax.plot(num_its, lfnp_perfs, label='leapfrog w/ naive pair')
    ax.plot(num_its, lfvl_perfs, label='leapfrog w/ verlet lists')
    ax.set_xlabel('Run Number')
    ax.set_ylabel('Running Time (secs)')
    ax.set_title('Running Time of MD simulations with ' + str(sim_steps) + ' Time Steps')
    ax.legend()
    
    lfnpp_average = lfnpp_tot / float(it)
    lfvlp_average = lfvlp_tot / float(it)
    
    print("Average runtime: leapfrog w/ naive pair, {} runs, {} time steps = {}".format(it, sim_steps, lfnpp_average))
    print()
    print("Average runtime: leapfrog w/ verlet lists, {} runs, {} time steps = {}".format(it, sim_steps, lfvlp_average))
    print()
    