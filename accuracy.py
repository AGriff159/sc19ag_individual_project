#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module containing functions which assist in computing accuracy-related metrics

@author: Aaron Griffin
"""

def total_energy_avg_diff(total_energies):
    
    diff = 0.0
    total_diff = 0.0
    avg_diff = 0.0
    te_num = len(total_energies)
    
    first_te = total_energies[0]
    
    for value in total_energies:
        diff = value - first_te
        total_diff += diff
        
    avg_diff = total_diff / float(te_num)
    
    return avg_diff


def calc_avg(values):
    avg = 0.0
    total = 0.0
    length = len(values)
    
    for val in values:
        total += val
    
    avg = total / length
    
    return avg