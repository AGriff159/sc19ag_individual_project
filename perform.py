#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module for computing the empirical running time measurements of each of the 
MD simulations which use the different algorithms of concern. 

@author: Aaron Griffin
"""

import md 
import time as t


def verlet_perf(N, X, Y, delta_t, steps):
    initial_time = t.perf_counter()
    ip, fp, iv, fv, cv, te = md.verlet_md(N,X,Y,delta_t,steps)
    final_time = t.perf_counter()

    return final_time - initial_time


def velocity_verlet_perf(N, X, Y, delta_t, steps):
    initial_time = t.perf_counter()
    ip, fp, iv, fv, cv, te = md.velocity_verlet_md(N, X, Y, delta_t, steps)
    final_time = t.perf_counter()

    return final_time - initial_time


def leapfrog_perf(N, X, Y, delta_t, steps):
    initial_time = t.perf_counter()
    ip, fp, iv, fv, cv, te = md.leapfrog_md(N, X, Y, delta_t, steps)
    final_time = t.perf_counter()

    return final_time - initial_time


def euler_cromer_perf(N, X, Y, delta_t, steps):
    initial_time = t.perf_counter()
    ip, fp, iv, fv, cv, te = md.euler_cromer_md(N,X,Y,delta_t,steps)
    final_time = t.perf_counter()

    return final_time - initial_time


def leapfrog_naive_pair_perf(N, X, Y, delta_t, steps, r_c):
    initial_time = t.perf_counter()
    ip, fp, iv, fv, cv, te = md.leapfrog_md_naive_pair(N,X,Y,delta_t,steps,r_c)
    final_time = t.perf_counter()

    return final_time - initial_time


def leapfrog_verlet_list_perf(N, X, Y, delta_t, steps, r_c, r_s):
    initial_time = t.perf_counter()
    ip, fp, iv, fv, cv, te = md.leapfrog_md_verlet_list(N, X, Y, delta_t, steps, r_c, r_s)
    final_time = t.perf_counter()

    return final_time - initial_time