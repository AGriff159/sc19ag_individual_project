# Software library written for final report experiments and results

## main.py

Please note that the file main.py contains example driver code to be commented/uncommented, 
with the purpose of testing the functions present within the other files. main.py is not part
of the library itself.

## Other files

The other files all constitute the library itself. The purpose of each module and its 
contained functions is detailed in a comment at the very top of each file. 

