#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module containing function for use with the Naive-Pair algorithm

@author: Aaron Griffin
"""

def naive_pair_test(r_ij, r_c):
    if r_ij < r_c:
        return True
    else:
        return False