#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Primary module of the library, which contains the functions which implement the 
MD simulations themselves, as well as the computations of metrics related to these
simulations as part of the simulations themselves.

@author: Aaron Griffin
"""


import random as rand
import math
import sympy as smp
import numpy as np
import sys
import sri

# All particles are assumed to have the same mass
mass = 1

# Parameters required for the Lennard-Jones Potential equation
sigma = 1.0
epsilon = 1.0

'''
Computes the force between two particles symbolically, given the distance between them, r.
Computes the negative derivative of the interaction potential U(r) with respect to r. 
The interaction potential equation used is the Lennard-Jones Potential.

Parameters:
    r_value = the numerical value of r

Returns:
    f = the calculated force given r
'''
def force(r_value):
    r = smp.symbols('r', real=True)
    minus_ur = -(4 * epsilon * ((sigma/r)**12 - (sigma/r)**6)) # the Lennard-Jones Potential Equation (negated)
    
    # symbolically derive the derivative and substitute in the values. Compute the result. 
    f = smp.diff(minus_ur, r)
    f = f.subs([(r, r_value)]).evalf()
    
    return f


'''
Implements a molecular dynamics simulation that uses the basic verlet algorithm as its
chosen numerical integrator.

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over
    new_positions = by default, an unused argument. If used, it must be a list 
                    containing the final positions of a previous verlet_md call
    
Returns:
    init_positions = Initial positions of the particles 
    positions = final positions of the particles after a number of steps, given by the parameter "steps"
'''
def verlet_md(N, X, Y, delta_t, steps, new_positions=[]):
    
    # check all particles will fit inside simulation box, as according to their 
    # initial arrangement
    if N > math.floor(X/2) * math.floor(Y/2):
        print("Error: Too many particles for the simulation box size given.")
        print("N must not exceed floor(X/2) * floor(Y/2)")
        print()
        sys.exit(1)
   
    # initially set each particle to their respective position in a 2D lattice 
    # structure sequentially
    x_positions = []
    y_positions = []
    for csy in np.arange(1, Y, 2):
        for csx in np.arange(1, X, 2):
            x_positions.append(csx)
            y_positions.append(csy)
            
            
    if not new_positions: 
        
        positions = [ [x_positions[n], y_positions[n]] for n in range(N)] 
    else:
        
        positions = []
        for n in range(N):
            positions.append( [new_positions[n][0], new_positions[n][1]] )
            
    
    init_positions = []
    for n in range(N):
        init_positions.append([positions[n][0],positions[n][1]])
        
    
    # randomise the initial velocities
    init_velocities = []
    velocities = []
    rand_lb = 0.00000001
    rand_ub = 0.0000001
    for n in range(N):
        velocities.append( [rand.uniform(rand_lb, rand_ub), rand.uniform(rand_lb, rand_ub)] )
        init_velocities.append([velocities[n][0],velocities[n][1]])
    
    # initialise to zero the acceleration values of all particles, in both the 
    # x and y directions
    accelerations = []
    for n in range(N):
        accelerations.append([0.0,0.0])
    
    # set up a list to keep track of the computed positions of all the particles 
    # at the previous time step, for use in the basic verlet equation
    positions_prev_step = []
    for n in range(N):
        positions_prev_step.append([0.0,0.0])
   
        
    # initialise the vector which will track the velocity at the centre of mass of the 
    # system
    centre_velocity = [0.0,0.0]
        
    total_energies = []
    for ts in range(steps):
        total_energies.append(0.0)
    te_i = 0
    
    for t in range(steps):
        
        velocities_squared = []
        for j in range(N):
            velocities_squared.append(0.0)
        vs_i = 0
        
        len_jones_pot = []
        for k in range( N*(N-1) ):
            len_jones_pot.append(0.0)
        ljp_i = 0
        
        for i in range(N):
            for j in range(N):
                
                if i == j: continue
            
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                len_jones_pot[ljp_i] = 4.0 * epsilon * ((sigma/r_ij)**12 - (sigma/r_ij)**6)
                ljp_i += 1
                
        
        # Compute total potential energy for the current time step, t
        len_jones_pot_total = 0.0
        for value in len_jones_pot:
            len_jones_pot_total += value
            
        total_pe = len_jones_pot_total / 2.0
        
       
        # calculation of forces between particles
        forces = []
        for n in range(N):
            forces.append([0.0,0.0])
        
        for i in range(N):
            for j in range(N):

                # Only need to consider i<j
                if i>=j: continue

                # Compute the distance between the two particles. 
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                # To avoid division by zero errors, impose a min value on r.
                r_ij = max(r_ij, sigma)
                
                # Get a unit vector, pointing from particle i to particle j.
                t_ij = [(positions[j][0]-positions[i][0])/r_ij, (positions[j][1]-positions[i][1])/r_ij]

                f = force(r_ij)  # compute the force between i and j

                # The force between i and j is actually a vector, so convert it 
                # to a vector by multiplying its value by the unit vector just computed. 
                forceVec = [t_ij[0]*f, t_ij[1]*f]

                # Add to the total force on particle i.
                forces[i][0] += forceVec[0]
                forces[i][1] += forceVec[1] 

                # Also subtract from the total force for particle i. This uses Newton's third law.
                forces[j][0] -= forceVec[0]
                forces[j][1] -= forceVec[1] 
        
        # Now the forces have been computed, calculate accelerations and then positions
        # using basic verlet
        for i in range(N):
            accelerations[i][0] = forces[i][0] / mass 
            accelerations[i][1] = forces[i][1] / mass
            
            # ensure that the current position values in one equation are not affected by the results of other equations
            ix_position = positions[i][0]
            iy_position = positions[i][1]
            
            # implementation of basic verlet equation to compute positions of all particles at the next time step
            if t == 0:
                positions[i][0] = ix_position + velocities[i][0] * delta_t + 0.5 * accelerations[i][0] * (delta_t**2)
                positions[i][1] = iy_position + velocities[i][1] * delta_t + 0.5 * accelerations[i][1] * (delta_t**2)
                positions_prev_step[i][0] = ix_position
                positions_prev_step[i][1] = iy_position 
            else:
                positions[i][0] = 2*ix_position - positions_prev_step[i][0] + accelerations[i][0] * (delta_t**2)
                positions[i][1] = 2*iy_position - positions_prev_step[i][1] + accelerations[i][1] * (delta_t**2)
                positions_prev_step[i][0] = ix_position 
                positions_prev_step[i][1] = iy_position
                
            # update velocities
            velocities[i][0] = (positions[i][0] - positions_prev_step[i][0]) / 2 * delta_t
            velocities[i][1] = (positions[i][1] - positions_prev_step[i][1]) / 2 * delta_t 
            
            # calculate velocity at centre of mass of the system at current time step, t
            centre_velocity[0] += velocities[i][0]
            centre_velocity[1] += velocities[i][1]
            
            
            velocities_squared[vs_i] = velocities[i][0]**2 + velocities[i][1]**2  # as m = 1, can be omitted here
            vs_i += 1
        
        # Compute total kinetic energy at the current time step, t
        total = 0.0
        for value in velocities_squared:
            total += value
        
        total_ke = total / 2.0
        
        # Compute total energy at the current time step, t. Add this to list of total energies at each time step
        total_energies[te_i] = total_ke + total_pe
        te_i += 1
            
            
    return init_positions, positions, init_velocities, velocities, centre_velocity, total_energies

    
'''
Prints the textual results of an md simulation that uses the basic verlet algorithm as its
numerical integrator.  

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over

Returns:
    No return value(s)
'''
def verlet_md_text(N, X, Y, delta_t, steps):
    
    init_positions, final_positions, init_velocities, final_velocities, centre_velocity, total_energies = verlet_md(N, X, Y, delta_t, steps)
    
    # print initial configuration to console textually
    print("Initial positions and Velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,init_positions[n][0],init_positions[n][1],init_velocities[n][0],init_velocities[n][1]))
    print()
                        
    # print final configuration to console textually
    print("Final positions and Velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,final_positions[n][0],final_positions[n][1],final_velocities[n][0],final_velocities[n][1]))
    print()
    
    print("Final Velocity at Centre of Mass = {}".format(centre_velocity))
    print()
    
    print("Total Energy at each time step:")
    for ts in range(steps):
        print("Time step {}: {}".format(ts, total_energies[ts]))



'''
Implements a molecular dynamics simulation that uses the velocity verlet algorithm as its
chosen numerical integrator.

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over
    new_positions = by default, an unused argument. If used, it must be a list 
                    containing the final positions of a previous velocity_verlet_md call
    
Returns:
    init_positions = Initial positions of the particles 
    positions = final positions of the particles after a number of time steps, given by the parameter "steps"
    init_velocities = Initial velocities of the particles
    velocities = final velocities of the particles after "steps" number of time steps
    centre_velocity = The final velocity at the centre of mass of the system once the simulation has 
                      finished running
    total_energies = a list where each element is the total energy of the system at each time step
'''
def velocity_verlet_md(N, X, Y, delta_t, steps, new_positions=[]):
    
    # check all particles will fit inside simulation box, as according to their 
    # initial arrangement
    if N > math.floor(X/2) * math.floor(Y/2):
        print("Error: Too many particles for the simulation box size given.")
        print("N must not exceed floor(X/2) * floor(Y/2)")
        print()
        sys.exit(1)
   
    # initially set each particle to their respective position in a 2D lattice 
    # structure sequentially
    x_positions = []
    y_positions = []
    for csy in np.arange(1, Y, 2):
        for csx in np.arange(1, X, 2):
            x_positions.append(csx)
            y_positions.append(csy)
            
            
    if not new_positions: 
        
        positions = [ [x_positions[n], y_positions[n]] for n in range(N)] 
    else:
        
        positions = []
        for n in range(N):
            positions.append( [new_positions[n][0], new_positions[n][1]] )
            
    
    init_positions = []
    for n in range(N):
        init_positions.append([positions[n][0],positions[n][1]])
        
    
    # randomise the initial velocities
    init_velocities = []
    velocities = []
    rand_lb = 0.00000001
    rand_ub = 0.0000001
    for n in range(N):
        velocities.append( [rand.uniform(rand_lb, rand_ub), rand.uniform(rand_lb, rand_ub)] )
        init_velocities.append([velocities[n][0],velocities[n][1]])
    
    accelerations = []
    for n in range(N):
        accelerations.append([0.0,0.0])
        
        
    # initialise the vector which will track the velocity at the centre of mass of the 
    # system
    centre_velocity = [0.0,0.0]
        
    total_energies = []
    for ts in range(steps):
        total_energies.append(0.0)
    te_i = 0
    
    for t in range(steps):
        
        velocities_squared = []
        for j in range(N):
            velocities_squared.append(0.0)
        vs_i = 0
        
        len_jones_pot = []
        for k in range( N*(N-1) ):
            len_jones_pot.append(0.0)
        ljp_i = 0
        
        for i in range(N):
            for j in range(N):
                
                if i == j: continue
            
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                len_jones_pot[ljp_i] = 4.0 * epsilon * ((sigma/r_ij)**12 - (sigma/r_ij)**6)
                ljp_i += 1
                
        
        # Compute total potential energy for the current time step, t
        len_jones_pot_total = 0.0
        for value in len_jones_pot:
            len_jones_pot_total += value
            
        total_pe = len_jones_pot_total / 2.0
        
       
        # calculation of forces between particles
        forces = []
        for n in range(N):
            forces.append([0.0,0.0]) 
       
        
        for i in range(N):
            for j in range(N):

                if i>=j: continue

                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                r_ij = max(r_ij, sigma)
                
                t_ij = [(positions[j][0]-positions[i][0])/r_ij, (positions[j][1]-positions[i][1])/r_ij]
                
                f = force(r_ij)
                
                forceVec = [t_ij[0]*f, t_ij[1]*f]

                forces[i][0] += forceVec[0]
                forces[i][1] += forceVec[1] 
                forces[j][0] -= forceVec[0]
                forces[j][1] -= forceVec[1] 
        
        # Now the forces have been computed, calculate positions and velocities 
        # of the particles at the next time step
        for i in range(N):
            current_acc_xi = accelerations[i][0]
            current_acc_yi = accelerations[i][1]
            
            accelerations[i][0] = forces[i][0] / mass
            accelerations[i][1] = forces[i][1] / mass
            
            positions[i][0] = positions[i][0] + velocities[i][0] * delta_t + (current_acc_xi/2) * delta_t**2
            positions[i][1] = positions[i][1] + velocities[i][1] * delta_t + (current_acc_yi/2) * delta_t**2
            
            velocities[i][0] = velocities[i][0] + ((current_acc_xi + accelerations[i][0]) / 2) * delta_t
            velocities[i][1] = velocities[i][1] + ((current_acc_yi + accelerations[i][1]) / 2) * delta_t
            
            centre_velocity[0] += velocities[i][0]
            centre_velocity[1] += velocities[i][1]
            
            velocities_squared[vs_i] = velocities[i][0]**2 + velocities[i][1]**2  # as m = 1, can be omitted here
            vs_i += 1
        
        # Compute total kinetic energy at the current time step, t
        total = 0.0
        for value in velocities_squared:
            total += value
        
        total_ke = total / 2.0
        
        # Compute total energy at the current time step, t. Add this to list of total energies at each time step
        total_energies[te_i] = total_ke + total_pe
        te_i += 1
        
    
    return init_positions, positions, init_velocities, velocities, centre_velocity, total_energies




'''
Prints the textual results of an md simulation that uses the velocity verlet algorithm as its
numerical integrator.  

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over

Returns:
    No return value(s)
'''
def velocity_verlet_md_text(N, X, Y, delta_t, steps):
      
    init_positions, final_positions, init_velocities, final_velocities, centre_velocity, total_energies = velocity_verlet_md(N, X, Y, delta_t, steps)
        
    print("Initial positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,init_positions[n][0],init_positions[n][1],init_velocities[n][0],init_velocities[n][1]))
    print()
    
    print("Final positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,final_positions[n][0],final_positions[n][1],final_velocities[n][0],final_velocities[n][1]))
    print()
    
    print("Final Velocity at Centre of Mass = {}".format(centre_velocity))
    print()
    
    print("Total Energy at each time step:")
    for ts in range(steps):
        print("Time step {}: {}".format(ts, total_energies[ts]))




'''
Implements a molecular dynamics simulation that uses the euler-cromer algorithm as its
chosen numerical integrator.

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over
    new_positions = by default, an unused argument. If used, it must be a list 
                    containing the final positions of a previous velocity_verlet_md call
    
Returns:
    init_positions = Initial positions of the particles 
    positions = final positions of the particles after a number of time steps, given by the parameter "steps"
    init_velocities = Initial velocities of the particles
    velocities = final velocities of the particles after "steps" number of time steps
    centre_velocity = The final velocity at the centre of mass of the system once the simulation has 
                      finished running
    total_energies = a list where each element is the total energy of the system at each time step
'''
def euler_cromer_md(N, X, Y, delta_t, steps, new_positions=[]):
    
    # check all particles will fit inside simulation box, as according to their 
    # initial arrangement
    if N > math.floor(X/2) * math.floor(Y/2):
        print("Error: Too many particles for the simulation box size given.")
        print("N must not exceed floor(X/2) * floor(Y/2)")
        print()
        sys.exit(1)
    
    # initially set each particle to their respective position in a 2D lattice 
    # structure sequentially
    x_positions = []
    y_positions = []
    for csy in np.arange(1, Y, 2):
        for csx in np.arange(1, X, 2):
            x_positions.append(csx)
            y_positions.append(csy)
        
    
    if not new_positions:  
        
        positions = [ [x_positions[n], y_positions[n] ] for n in range(N)]
    else:
        
        positions = []
        for n in range(N):
            positions.append( [new_positions[n][0], new_positions[n][1]] )
            
    
    init_positions = []
    for n in range(N):
        init_positions.append([positions[n][0],positions[n][1]]) 
        
    
    # randomise the initial velocities
    init_velocities = []
    velocities = []
    rand_lb = 0.00000001
    rand_ub = 0.0000001
    for n in range(N):
        velocities.append( [rand.uniform(rand_lb, rand_ub), rand.uniform(rand_lb, rand_ub)] )
        init_velocities.append([velocities[n][0],velocities[n][1]])
    
    accelerations = []
    for n in range(N):
        accelerations.append([0.0,0.0])
    
    centre_velocity = [0.0,0.0]
    
    total_energies = []
    for ts in range(steps):
        total_energies.append(0.0)
    te_i = 0
        
    
    for t in range(steps):
       
        velocities_squared = []
        for j in range(N):
            velocities_squared.append(0.0)
        vs_i = 0
        
        len_jones_pot = []
        for k in range( N*(N-1) ):
            len_jones_pot.append(0.0)
        ljp_i = 0
        
        for i in range(N):
            for j in range(N):
                
                if i == j: continue
            
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                len_jones_pot[ljp_i] = 4.0 * epsilon * ((sigma/r_ij)**12 - (sigma/r_ij)**6)
                ljp_i += 1
                
        
        # Compute total potential energy for the current time step, t
        len_jones_pot_total = 0.0
        for value in len_jones_pot:
            len_jones_pot_total += value
            
        total_pe = len_jones_pot_total / 2.0
        
       
        # calculation of forces between particles
        forces = []
        for n in range(N):
            forces.append([0.0,0.0]) 
       
        
        for i in range(N):
            for j in range(N):

                if i>=j: continue

                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                r_ij = max(r_ij, sigma)
                
                t_ij = [(positions[j][0]-positions[i][0])/r_ij, (positions[j][1]-positions[i][1])/r_ij]

                f = force(r_ij)  

                forceVec = [t_ij[0]*f, t_ij[1]*f]
                
                forces[i][0] += forceVec[0]
                forces[i][1] += forceVec[1] 
                forces[j][0] -= forceVec[0]
                forces[j][1] -= forceVec[1] 
              
        for i in range(N):
            velocities[i][0] = velocities[i][0] + accelerations[i][0] * delta_t
            velocities[i][1] = velocities[i][1] + accelerations[i][1] * delta_t
            
            positions[i][0] = positions[i][0] + velocities[i][0] * delta_t
            positions[i][1] = positions[i][1] + velocities[i][1] * delta_t
            
            accelerations[i][0] = forces[i][0] / mass
            accelerations[i][1] = forces[i][1] / mass
            
            centre_velocity[0] += velocities[i][0]
            centre_velocity[1] += velocities[i][1]
            
            velocities_squared[vs_i] = velocities[i][0]**2 + velocities[i][1]**2  # as m = 1, can be omitted here
            vs_i += 1
            
        
        # Compute total kinetic energy at the current time step, t
        total = 0.0
        for value in velocities_squared:
            total += value
        
        total_ke = total / 2.0
        
        # Compute total energy at the current time step, t. Add this to list of total energies at each time step
        total_energies[te_i] = total_ke + total_pe
        te_i += 1
            
    
    return init_positions, positions, init_velocities, velocities, centre_velocity, total_energies


'''
Prints the textual results of an md simulation that uses the euler-cromer algorithm as its
numerical integrator.  

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over

Returns:
    No return value(s)
'''
def euler_cromer_md_text(N, X, Y, delta_t, steps):
    
    init_positions, final_positions, init_velocities, final_velocities, centre_velocity, total_energies = euler_cromer_md(N, X, Y, delta_t, steps)
        
    print("Initial positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,init_positions[n][0],init_positions[n][1],init_velocities[n][0],init_velocities[n][1]))
    print()
    
    print("Final positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,final_positions[n][0],final_positions[n][1],final_velocities[n][0],final_velocities[n][1]))
    print()
    
    print("Final Velocity at Centre of Mass = {}".format(centre_velocity))
    print()
    
    print("Total Energy at each time step:")
    for ts in range(steps):
        print("Time step {}: {}".format(ts, total_energies[ts]))
        


'''
Implements a molecular dynamics simulation that uses the leapfrog algorithm as its
chosen numerical integrator.

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over
    new_positions = by default, an unused argument. If used, it must be a list 
                    containing the final positions of a previous velocity_verlet_md call
    
Returns:
    init_positions = Initial positions of the particles 
    positions = final positions of the particles after a number of time steps, given by the parameter "steps"
    init_velocities = Initial velocities of the particles
    velocities = final velocities of the particles after "steps" number of time steps
    centre_velocity = The final velocity at the centre of mass of the system once the simulation has 
                      finished running
    total_energies = a list where each element is the total energy of the system at each time step
'''
def leapfrog_md(N, X, Y, delta_t, steps, new_positions=[]):
    
    # check all particles will fit inside simulation box, as according to their 
    # initial arrangement
    if N > math.floor(X/2) * math.floor(Y/2):
        print("Error: Too many particles for the simulation box size given.")
        print("N must not exceed floor(X/2) * floor(Y/2)")
        print()
        sys.exit(1)
   
    # initially set each particle to their respective position in a 2D lattice 
    # structure sequentially
    x_positions = []
    y_positions = []
    for csy in np.arange(1, Y, 2):
        for csx in np.arange(1, X, 2):
            x_positions.append(csx)
            y_positions.append(csy)
            
            
    if not new_positions: 
        
        positions = [ [x_positions[n], y_positions[n]] for n in range(N)] 
    else:
        
        positions = []
        for n in range(N):
            positions.append( [new_positions[n][0], new_positions[n][1]] )
            
    
    init_positions = []
    for n in range(N):
        init_positions.append([positions[n][0],positions[n][1]])
        
    
    # randomise the initial velocities
    init_velocities = []
    velocities = []
    rand_lb = 0.00001
    rand_ub = 0.0001
    for n in range(N):
        velocities.append( [rand.uniform(rand_lb, rand_ub), rand.uniform(rand_lb, rand_ub)] )
        init_velocities.append([velocities[n][0],velocities[n][1]])
    
    accelerations = []
    for n in range(N):
        accelerations.append([0.0,0.0])
        
    half_velocities = []
    for n in range(N):
        half_velocities.append([0.0,0.0])
        
        
    # initialise the vector which will track the velocity at the centre of mass of the 
    # system
    centre_velocity = [0.0,0.0]
        
    total_energies = []
    for ts in range(steps):
        total_energies.append(0.0)
    te_i = 0
    
    for t in range(steps):
        
        velocities_squared = []
        for j in range(N):
            velocities_squared.append(0.0)
        vs_i = 0
        
        len_jones_pot = []
        for k in range( N*(N-1) ):
            len_jones_pot.append(0.0)
        ljp_i = 0
        
        for i in range(N):
            for j in range(N):
                
                if i == j: continue
            
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                len_jones_pot[ljp_i] = 4.0 * epsilon * ((sigma/r_ij)**12 - (sigma/r_ij)**6)
                ljp_i += 1
                
        
        # Compute total potential energy for the current time step, t
        len_jones_pot_total = 0.0
        for value in len_jones_pot:
            len_jones_pot_total += value
            
        total_pe = len_jones_pot_total / 2.0
        
       
        # calculation of forces between particles
        forces = []
        for n in range(N):
            forces.append([0.0,0.0]) 
       
        
        for i in range(N):
            for j in range(N):

                if i>=j: continue

                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                r_ij = max(r_ij, sigma)
                
                t_ij = [(positions[j][0]-positions[i][0])/r_ij, (positions[j][1]-positions[i][1])/r_ij]
                
                f = force(r_ij)
                
                forceVec = [t_ij[0]*f, t_ij[1]*f]

                forces[i][0] += forceVec[0]
                forces[i][1] += forceVec[1] 
                forces[j][0] -= forceVec[0]
                forces[j][1] -= forceVec[1] 
        
        # Now the forces have been computed, calculate positions and velocities 
        # of the particles at the next time step
        for i in range(N):
            accelerations[i][0] = forces[i][0] / mass
            accelerations[i][1] = forces[i][1] / mass
            
            prev_hv_x = half_velocities[i][0]
            prev_hv_y = half_velocities[i][1]
            
            half_velocities[i][0] = prev_hv_x + accelerations[i][0] * delta_t
            half_velocities[i][1] = prev_hv_y + accelerations[i][1] * delta_t
            
            positions[i][0] = positions[i][0] + half_velocities[i][0] * delta_t
            positions[i][1] = positions[i][1] + half_velocities[i][1] * delta_t
            
            velocities[i][0] = (half_velocities[i][0] + prev_hv_x) / 2
            velocities[i][1] = (half_velocities[i][1] + prev_hv_y) / 2
            
            centre_velocity[0] += velocities[i][0]
            centre_velocity[1] += velocities[i][1]
            
            velocities_squared[vs_i] = velocities[i][0]**2 + velocities[i][1]**2  # as m = 1, can be omitted here
            vs_i += 1
        
        # Compute total kinetic energy at the current time step, t
        total = 0.0
        for value in velocities_squared:
            total += value
        
        total_ke = total / 2.0
        
        # Compute total energy at the current time step, t. Add this to list of total energies at each time step
        total_energies[te_i] = total_ke + total_pe
        te_i += 1
        
    
    return init_positions, positions, init_velocities, velocities, centre_velocity, total_energies




'''
Prints the textual results of an md simulation that uses the leapfrog algorithm as its
numerical integrator.  

Parameters:
    N = number of particles to be simulated, which cannot exceed floor(X/2) * floor(Y/2), 
    as beyond this threshold along both axes the particles will no longer fit in the 
    simulation box, causing the simulation to throw an error and fail to run
    X = the width of the simulation box
    Y = the height of the simulation box
    delta_t = the time step
    steps = number of steps to integrate over

Returns:
    No return value(s)
'''
def leapfrog_md_text(N, X, Y, delta_t, steps):
      
    init_positions, final_positions, init_velocities, final_velocities, centre_velocity, total_energies = leapfrog_md(N, X, Y, delta_t, steps)
        
    print("Initial positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,init_positions[n][0],init_positions[n][1],init_velocities[n][0],init_velocities[n][1]))
    print()
    
    print("Final positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,final_positions[n][0],final_positions[n][1],final_velocities[n][0],final_velocities[n][1]))
    print()
    
    print("Final Velocity at Centre of Mass = {}".format(centre_velocity))
    print()
    
    print("Total Energy at each time step:")
    for ts in range(steps):
        print("Time step {}: {}".format(ts, total_energies[ts]))



def leapfrog_md_naive_pair(N, X, Y, delta_t, steps, r_c, new_positions=[]):
    
    # check all particles will fit inside simulation box, as according to their 
    # initial arrangement
    if N > math.floor(X/2) * math.floor(Y/2):
        print("Error: Too many particles for the simulation box size given.")
        print("N must not exceed floor(X/2) * floor(Y/2)")
        print()
        sys.exit(1)
   
    # initially set each particle to their respective position in a 2D lattice 
    # structure sequentially
    x_positions = []
    y_positions = []
    for csy in np.arange(1, Y, 2):
        for csx in np.arange(1, X, 2):
            x_positions.append(csx)
            y_positions.append(csy)
            
            
    if not new_positions: 
        
        positions = [ [x_positions[n], y_positions[n]] for n in range(N)] 
    else:
        
        positions = []
        for n in range(N):
            positions.append( [new_positions[n][0], new_positions[n][1]] )
            
    
    init_positions = []
    for n in range(N):
        init_positions.append([positions[n][0],positions[n][1]])
        
    
    # randomise the initial velocities
    init_velocities = []
    velocities = []
    rand_lb = 0.00000001
    rand_ub = 0.0000001
    for n in range(N):
        velocities.append( [rand.uniform(rand_lb, rand_ub), rand.uniform(rand_lb, rand_ub)] )
        init_velocities.append([velocities[n][0],velocities[n][1]])
    
    # initialise to zero the acceleration values of all particles, in both the 
    # x and y directions
    accelerations = []
    for n in range(N):
        accelerations.append([0.0,0.0])
    
    half_velocities = []
    for n in range(N):
        half_velocities.append([0.0,0.0])
   
        
    # initialise the vector which will track the velocity at the centre of mass of the 
    # system
    centre_velocity = [0.0,0.0]
        
    total_energies = []
    for ts in range(steps):
        total_energies.append(0.0)
    te_i = 0
    
    for t in range(steps):
        
        velocities_squared = []
        for j in range(N):
            velocities_squared.append(0.0)
        vs_i = 0
        
        len_jones_pot = []
        for k in range( N*(N-1) ):
            len_jones_pot.append(0.0)
        ljp_i = 0
        
        for i in range(N):
            for j in range(N):
                
                if i == j: continue
            
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                len_jones_pot[ljp_i] = 4.0 * epsilon * ((sigma/r_ij)**12 - (sigma/r_ij)**6)
                ljp_i += 1
                
        
        # Compute total potential energy for the current time step, t
        len_jones_pot_total = 0.0
        for value in len_jones_pot:
            len_jones_pot_total += value
            
        total_pe = len_jones_pot_total / 2.0
        
       
        # calculation of forces between particles
        forces = []
        for n in range(N):
            forces.append([0.0,0.0])
        
        for i in range(N):
            for j in range(N):

                # Only need to consider i<j
                if i>=j: continue

                # Compute the distance between the two particles. 
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                # To avoid division by zero errors, impose a min value on r.
                r_ij = max(r_ij, sigma)
                
                if not(sri.naive_pair_test(r_ij, r_c)):
                    continue
                
                # Get a unit vector, pointing from particle i to particle j.
                t_ij = [(positions[j][0]-positions[i][0])/r_ij, (positions[j][1]-positions[i][1])/r_ij]

                f = force(r_ij)  # compute the force between i and j

                # The force between i and j is actually a vector, so convert it 
                # to a vector by multiplying its value by the unit vector just computed. 
                forceVec = [t_ij[0]*f, t_ij[1]*f]

                # Add to the total force on particle i.
                forces[i][0] += forceVec[0]
                forces[i][1] += forceVec[1] 

                # Also subtract from the total force for particle i. This uses Newton's third law.
                forces[j][0] -= forceVec[0]
                forces[j][1] -= forceVec[1] 
        
        # Now the forces have been computed, calculate accelerations and then positions
        # using the leapfrog algorithm
        for i in range(N):
            accelerations[i][0] = forces[i][0] / mass
            accelerations[i][1] = forces[i][1] / mass
            
            prev_hv_x = half_velocities[i][0]
            prev_hv_y = half_velocities[i][1]
            
            half_velocities[i][0] = prev_hv_x + accelerations[i][0] * delta_t
            half_velocities[i][1] = prev_hv_y + accelerations[i][1] * delta_t
            
            positions[i][0] = positions[i][0] + half_velocities[i][0] * delta_t
            positions[i][1] = positions[i][1] + half_velocities[i][1] * delta_t
            
            velocities[i][0] = (half_velocities[i][0] + prev_hv_x) / 2
            velocities[i][1] = (half_velocities[i][1] + prev_hv_y) / 2 
            
            # calculate velocity at centre of mass of the system at current time step, t
            centre_velocity[0] += velocities[i][0]
            centre_velocity[1] += velocities[i][1]
            
            
            velocities_squared[vs_i] = velocities[i][0]**2 + velocities[i][1]**2  # as m = 1, can be omitted here
            vs_i += 1
        
        # Compute total kinetic energy at the current time step, t
        total = 0.0
        for value in velocities_squared:
            total += value
        
        total_ke = total / 2.0
        
        # Compute total energy at the current time step, t. Add this to list of total energies at each time step
        total_energies[te_i] = total_ke + total_pe
        te_i += 1
            
            
    return init_positions, positions, init_velocities, velocities, centre_velocity, total_energies


def leapfrog_md_naive_pair_text(N, X, Y, delta_t, steps, r_c):
      
    init_positions, final_positions, init_velocities, final_velocities, centre_velocity, total_energies = leapfrog_md_naive_pair(N, X, Y, delta_t, steps, r_c)
        
    print("Initial positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,init_positions[n][0],init_positions[n][1],init_velocities[n][0],init_velocities[n][1]))
    print()
    
    print("Final positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,final_positions[n][0],final_positions[n][1],final_velocities[n][0],final_velocities[n][1]))
    print()
    
    print("Final Velocity at Centre of Mass = {}".format(centre_velocity))
    print()
    
    print("Total Energy at each time step:")
    for ts in range(steps):
        print("Time step {}: {}".format(ts, total_energies[ts]))


def leapfrog_md_verlet_list(N, X, Y, delta_t, steps, r_c, r_s, new_positions=[]):
    
    # check all particles will fit inside simulation box, as according to their 
    # initial arrangement
    if N > math.floor(X/2) * math.floor(Y/2):
        print("Error: Too many particles for the simulation box size given.")
        print("N must not exceed floor(X/2) * floor(Y/2)")
        print()
        sys.exit(1)
   
    # initially set each particle to their respective position in a 2D lattice 
    # structure sequentially
    x_positions = []
    y_positions = []
    for csy in np.arange(1, Y, 2):
        for csx in np.arange(1, X, 2):
            x_positions.append(csx)
            y_positions.append(csy)
            
            
    if not new_positions: 
        
        positions = [ [x_positions[n], y_positions[n]] for n in range(N)] 
    else:
        
        positions = []
        for n in range(N):
            positions.append( [new_positions[n][0], new_positions[n][1]] )
            
    
    init_positions = []
    for n in range(N):
        init_positions.append([positions[n][0],positions[n][1]])
        
    
    # randomise the initial velocities
    init_velocities = []
    velocities = []
    rand_lb = 0.00001
    rand_ub = 0.0001
    for n in range(N):
        velocities.append( [rand.uniform(rand_lb, rand_ub), rand.uniform(rand_lb, rand_ub)] )
        init_velocities.append([velocities[n][0],velocities[n][1]])
    
    accelerations = []
    for n in range(N):
        accelerations.append([0.0,0.0])
        
    half_velocities = []
    for n in range(N):
        half_velocities.append([0.0,0.0])
        
        
    # initialise the vector which will track the velocity at the centre of mass of the 
    # system
    centre_velocity = [0.0,0.0]
        
    total_energies = []
    for ts in range(steps):
        total_energies.append(0.0)
    te_i = 0
  
    # define verlet lists for all particles
    verlet_lists = [[[1e15 for k in range(2)] for j in range(N)] for i in range(N)]       
            
    
    for t in range(steps):
        
        velocities_squared = []
        for j in range(N):
            velocities_squared.append(0.0)
        vs_i = 0
        
        len_jones_pot = []
        for k in range( N*(N-1) ):
            len_jones_pot.append(0.0)
        ljp_i = 0
        
        for i in range(N):
            for j in range(N):
                
                if i == j: continue
            
                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                
                len_jones_pot[ljp_i] = 4.0 * epsilon * ((sigma/r_ij)**12 - (sigma/r_ij)**6)
                ljp_i += 1
                
        
        # Compute total potential energy for the current time step, t
        len_jones_pot_total = 0.0
        for value in len_jones_pot:
            len_jones_pot_total += value
            
        total_pe = len_jones_pot_total / 2.0
        
       
        # calculation of forces between particles
        forces = []
        for n in range(N):
            forces.append([0.0,0.0]) 
       
        
        for i in range(N):
            for j in range(N):

                if i>=j: continue

                r_ij = math.sqrt( (positions[i][0]-positions[j][0])**2 + (positions[i][1]-positions[j][1])**2 )
                r_ij = max(r_ij, sigma)
                
                
                # update verlet list of each ith particle every ten time steps
                if t % 10 == 0:
                    if r_ij <= r_c + r_s:
                        for n in range(N):
                            for m in range(N):
                                for o in range(2):
                                    if o == 0:
                                        verlet_lists[n][m][o] = positions[j][0]
                                        verlet_lists[n][m][o+1] = positions[j][1]
                
                # every time step, check if each ith particle has the current neighbour in its verlet list
                # If not, do not compute the forces between these two particles
                check = False
                for n in range(N):
                    for m in range(N):
                        for o in range(2):
                           if o == 0:
                               if verlet_lists[n][m][o] == positions[j][0]:
                                   if verlet_lists[n][m][o+1] == positions[j][1]:
                                       check = True
                
                if check == False:
                    continue
                
                t_ij = [(positions[j][0]-positions[i][0])/r_ij, (positions[j][1]-positions[i][1])/r_ij]
                
                f = force(r_ij)
                
                forceVec = [t_ij[0]*f, t_ij[1]*f]

                forces[i][0] += forceVec[0]
                forces[i][1] += forceVec[1] 
                forces[j][0] -= forceVec[0]
                forces[j][1] -= forceVec[1] 
        
        # Now the forces have been computed, calculate positions and velocities 
        # of the particles at the next time step
        for i in range(N):
            accelerations[i][0] = forces[i][0] / mass
            accelerations[i][1] = forces[i][1] / mass
            
            prev_hv_x = half_velocities[i][0]
            prev_hv_y = half_velocities[i][1]
            
            half_velocities[i][0] = prev_hv_x + accelerations[i][0] * delta_t
            half_velocities[i][1] = prev_hv_y + accelerations[i][1] * delta_t
            
            positions[i][0] = positions[i][0] + half_velocities[i][0] * delta_t
            positions[i][1] = positions[i][1] + half_velocities[i][1] * delta_t
            
            velocities[i][0] = (half_velocities[i][0] + prev_hv_x) / 2
            velocities[i][1] = (half_velocities[i][1] + prev_hv_y) / 2
            
            centre_velocity[0] += velocities[i][0]
            centre_velocity[1] += velocities[i][1]
            
            velocities_squared[vs_i] = velocities[i][0]**2 + velocities[i][1]**2  # as m = 1, can be omitted here
            vs_i += 1
        
        # Compute total kinetic energy at the current time step, t
        total = 0.0
        for value in velocities_squared:
            total += value
        
        total_ke = total / 2.0
        
        # Compute total energy at the current time step, t. Add this to list of total energies at each time step
        total_energies[te_i] = total_ke + total_pe
        te_i += 1
        
    
    return init_positions, positions, init_velocities, velocities, centre_velocity, total_energies


def leapfrog_md_verlet_list_text(N, X, Y, delta_t, steps, r_c, r_s):
      
    init_positions, final_positions, init_velocities, final_velocities, centre_velocity, total_energies = leapfrog_md_verlet_list(N, X, Y, delta_t, steps, r_c, r_s)
        
    print("Initial positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,init_positions[n][0],init_positions[n][1],init_velocities[n][0],init_velocities[n][1]))
    print()
    
    print("Final positions and velocities:")
    for n in range(N):
        print("Particle {}:\tPosition (x,y)=({},{})\tVelocity (vx,vy)=({},{})".format(n,final_positions[n][0],final_positions[n][1],final_velocities[n][0],final_velocities[n][1]))
    print()
    
    print("Final Velocity at Centre of Mass = {}".format(centre_velocity))
    print()
    
    print("Total Energy at each time step:")
    for ts in range(steps):
        print("Time step {}: {}".format(ts, total_energies[ts]))
    


